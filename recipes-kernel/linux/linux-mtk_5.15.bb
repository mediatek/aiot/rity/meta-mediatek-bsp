# Copyright (C) 2022 Fabien Parent <fparent@baylibre.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require linux-mtk-common.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

LINUX_VERSION ?= "5.15.47"
SRCBRANCH ?= "mtk-v5.15-dev"
SRCREV = "d54a73189b6d73ee94de84752e0dbc0fe6588475"

