// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2024 MediaTek Inc.
 *
 */

#include <dt-bindings/clock/mt8195-clk.h>
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/pinctrl/mt8195-pinfunc.h>

/dts-v1/;
/plugin/;

#include "isp70.dtsi"

/ {
	fragment@3 {
		target-path = "/";
		__overlay__ {
			cam0_dvdd_en: cam0-dvdd-en-regulator {
				compatible = "regulator-fixed";
				regulator-name = "cam0_dvdd_en";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				gpio = <&pio 60 0>;
				startup-delay-us = <18000>;
				enable-active-high;
				pinctrl-names = "default";
				pinctrl-0 = <&cam0_dvdd_en_pins>;
			};

			cam0_avdd_en: cam0-avdd-en-regulator {
				compatible = "regulator-fixed";
				regulator-name = "cam0_avdd_en";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				gpio = <&pio 56 0>;
				startup-delay-us = <18000>;
				enable-active-high;
				pinctrl-names = "default";
				pinctrl-0 = <&cam0_avdd_en_pins>;
			};

			cam1_dvdd_en: cam1-dvdd-en-regulator {
				compatible = "regulator-fixed";
				regulator-name = "cam1_dvdd_en";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				gpio = <&pio 58 0>;
				startup-delay-us = <18000>;
				enable-active-high;
				pinctrl-names = "default";
				pinctrl-0 = <&cam1_dvdd_en_pins>;
			};

			cam_vcam_3v3_en: cam_vcam_3v3_en-regulator {
				compatible = "regulator-fixed";
				regulator-name = "cam_vcam_3v3_en";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				vin-supply = <&mt6359_vcamio_ldo_reg>;
				enable-active-high;
				regulator-always-on;
			};
		};
	};

	fragment@4 {
		target = <&pio>;
		__overlay__ {
			cam1_pins_default: cam1_pins_default {
				pins_cmd_dat {
					pinmux = <PINMUX_GPIO55__FUNC_GPIO55>,
						 <PINMUX_GPIO23__FUNC_CMMCLK1>;
				};
			};

			cam0_dvdd_en_pins: cam0-dvdd-en-pins {
				pins_cmd_dat {
					pinmux = <PINMUX_GPIO60__FUNC_GPIO60>;
				};
			};

			cam0_avdd_en_pins: cam0-avdd-en-pins {
				pins_cmd_dat {
					pinmux = <PINMUX_GPIO56__FUNC_GPIO56>;
				};
			};

			cam1_dvdd_en_pins: cam1-dvdd-en-pins {
				pins_cmd_dat {
					pinmux = <PINMUX_GPIO58__FUNC_GPIO58>;
				};
			};
		};
	};

	fragment@5 {
		target = <&i2c0>;
		__overlay__ {
			clock-frequency = <400000>;

			imx214@1a {
				compatible = "sony,imx214";

				reg = <0x1a>;
				status = "okay";

				pinctrl-names = "default";
				pinctrl-0 = <&cam1_pins_default>;

				clocks = <&topckgen CLK_TOP_CAMTG2>;
				assigned-clocks = <&topckgen CLK_TOP_CAMTG2>;
				assigned-clock-parents = <&topckgen CLK_TOP_UNIVPLL_192M_D8>;

				enable-gpios = <&pio 55 GPIO_ACTIVE_HIGH>;

				vdddo-supply = <&cam1_dvdd_en>;
				vddd-supply = <&cam0_dvdd_en>;
				vdda-supply = <&cam0_avdd_en>;

				port {
					sensor1_out: endpoint {
						remote-endpoint = <&seninf_csi_port_1_in>;
						data-lanes = <1 2 3 4>;
						link-frequencies = /bits/ 64 <480000000>;
					};
				};
			};
		};
	};

	fragment@6 {
		target = <&seninf_top>;
		__overlay__ {
			seninf_csi_port_1: seninf_csi_port_1 {
				compatible = "mediatek,seninf";
				csi-port = "1";

				port {
					seninf_csi_port_1_in: endpoint {
						remote-endpoint = <&sensor1_out>;
						data-lanes = <1 2 3 4>;
					};
				};
			};
		};
	};

	fragment@7 {
		target = <&imgsys_fw>;
		__overlay__ {
			status = "disabled";
		};
	};

	fragment@8 {
		target = <&hcp>;
		__overlay__ {
			status = "disabled";
		};
	};

	fragment@9 {
		target-path = "/soc/imgsys_larb";
		__overlay__ {
			status = "disabled";
		};
	};

	fragment@10 {
		target = <&ipesys_me>;
		__overlay__ {
			status = "disabled";
		};
	};
};
