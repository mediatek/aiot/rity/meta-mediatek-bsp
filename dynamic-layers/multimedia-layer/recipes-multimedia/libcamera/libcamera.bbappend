SRC_URI = "${AIOT_BSP_URI}/libcamera.git;protocol=https;branch=mtk-aiot-multi-stream"
SRCREV = "84a438b0675702703035a0737bfa4cdd2f3d527e"

PV = "202111+git${SRCPV}"

FILES:${PN}-dev += " ${libdir}/libcamera*.so"

PACKAGECONFIG[lc-compliance] = "-Dlc-compliance=enabled,-Dlc-compliance=disabled"
PACKAGECONFIG:append = " gst"

CXXFLAGS += "-Wno-error=deprecated-declarations"
