require u-boot-common_${PV}.inc
require recipes-bsp/u-boot/u-boot-tools.inc

SRC_URI:append:class-target = " \
    file://0001-tools-Remove-command-invocation-for-logo-data.patch \
"

SRC_URI:append = " \
    file://0002-libfdt-Fix-invalid-version-warning.patch \
"

DEPENDS += "gnutls openssl util-linux swig-native python3-setuptools-native"

do_install:append () {
	install -m 0755 tools/mkeficapsule ${D}${bindir}/uboot-mkeficapsule
	ln -sf uboot-mkeficapsule ${D}${bindir}/mkeficapsule

	install -m 0755 tools/fdt_add_pubkey ${D}${bindir}/uboot-fdt_add_pubkey
	ln -sf uboot-fdt_add_pubkey ${D}${bindir}/fdt_add_pubkey
}

FILES:${PN} += " \
    ${bindir}/uboot-mkeficapsule \
    ${bindir}/mkeficapsule \
    ${bindir}/uboot-fdt_add_pubkey \
    ${bindir}/fdt_add_pubkey \
"
