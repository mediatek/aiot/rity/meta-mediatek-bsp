DESCRIPTION = "MediaTek camera ISP7 out-of-tree kernel driver"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=6760c75814a7f07fc569c7ca1fd5c35b"

COMPATIBLE_MACHINE = "mt8395|mt8390|mt8370"

inherit module

SRCREV = "5e0d85663da71fe410d929203fe6a72e33c16fd1"

BRANCH = "mtk-v5.15"

SRC_URI += "${AIOT_BSP_URI}/mtk-camisp-driver.git;protocol=https;branch=${BRANCH} \
"

S = "${WORKDIR}/git"

# The inherit of module.bbclass will automatically name module packages with
# "kernel-module-" prefix as required by the oe-core build environment.

RPROVIDES_${PN} += "kernel-module-mtk-camisp-driver"

python() {
    plat = d.getVar('SOC_FAMILY', True)
    if plat == 'mt8370':
        plat = 'mt8188'
    d.setVar('PLATFORM', plat)
}

EXTRA_OEMAKE:append = " PLATFORM=${PLATFORM} "

do_install:append() {
    install -d ${D}${nonarch_base_libdir}/firmware/
    dd if=/dev/zero of=${D}${nonarch_base_libdir}/firmware/remoteproc_scp bs=1 count=100
}

FILES:${PN} += " \
    ${nonarch_base_libdir}/firmware/remoteproc_scp \
"
